**Working copy example of loadtest project:**

- **Scripts** - [JMeter](https://gitlab.com/testload/jmeter) scripts (JMX) for [Google's main page](https://www.google.com) "loadtesting" (just for example)
- **Profiles** - Load profiles for [Gitlab loadtest automation tool](https://gitlab.com/testload/sant-gitlab-ci)
- **Results** - Loadtest results published by [Gitlab loadtest automation tool](https://gitlab.com/testload/sant-gitlab-ci)
- **.gitlab-ci.yml** - Gitlab-ci execution scripts (start distributed loadtesting execution based on Profiles)

***public** - just experiment for autopublish profiles and results to [Gitlab Pages](http://examplescripts.testload.cloud) (it does not working properly, currently)*
